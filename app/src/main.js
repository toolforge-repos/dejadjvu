import { createApp } from 'vue'
import { createRouter, createMemoryHistory } from 'vue-router';
import App from './App.vue'

import './index.css'

const routes = [
    { path: "/", name: 'Main', component: () => import('./views/MainView.vue')},
    { path: "/create-book", name: 'Create Book', component: () => import('./views/CreateBookView.vue')}
]

const router = createRouter({
    history: createMemoryHistory(),
    routes: routes
})

createApp(App).use(router).mount('#app')
