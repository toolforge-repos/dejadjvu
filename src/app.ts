import express from 'express';
import path from 'path';

const app = express();
app.use(express.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, Content, Accept, Content-Type');
    res.setHeader('Access-Control-Allow-Methods', 'GET');
    next();
});

// Serving Vue app
const vueDir = path.join(__dirname, '/views/');
app.use(express.static(vueDir));

app.get('/', (req, res) => {
    res.sendFile(vueDir + 'index.html');
});

// API

app.use('/api/docs', require('./routes/docs'));
app.use('/api/tasks', require('./routes/tasks'));
app.use('/api/gallica', require('./routes/gallica'));

export { app };
