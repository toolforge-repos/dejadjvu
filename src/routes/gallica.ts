import express from 'express';

const router = express.Router();

const controller = require('../controllers/gallica');

router.get('/:id', controller.getManifest);

module.exports = router;
