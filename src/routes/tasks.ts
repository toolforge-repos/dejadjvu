import express from 'express';

const router = express.Router();
const controller = require('../controllers/tasks');

router.get('/', controller.getTasks);

module.exports = router;
