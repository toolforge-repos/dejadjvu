import express from 'express';

const router = express.Router();

const controller = require('../controllers/docs');

router.get('/', controller.getDocs);

module.exports = router;
