import type express from 'express';
import fs from 'fs';

exports.getDocs = (req: express.Request, res: express.Response) => {
    if (fs.existsSync('docs/out/index.html')) {
        res.sendFile('docs/out/index.html', { root: '.' });
    } else {
        res.status(404).send('<h1>Documentation not found.</h1>');
    }
};
