import type express from 'express';
import fetch from 'node-fetch-commonjs';
import z from 'zod';
import https from 'https';
import fs from 'fs';
import { execSync } from 'child_process';

const I3FCanvas = z.object({
    label: z.string(),
    images: z.array(z.object({
        motivation: z.string(),
        on: z.string(),
        resource: z.object({
            format: z.string(),
            height: z.number().positive(),
            width: z.number().positive(),
            '@id': z.string()
        })
    })).length(1, 'I3FManifest should only have 1 image per canvas')
});
type I3FCanvas = z.infer<typeof I3FCanvas>;
const I3FManifest = z.object({
    sequences: z.array(z.object({
        canvases: z.array(I3FCanvas)
    }))
});
type I3FManifest = z.infer<typeof I3FManifest>;

const download = async (url: string, dest: string) => {
    const file = fs.createWriteStream(dest);
    https.get(url, {
        headers: {
            'User-Agent': 'Dejadjvu 0.1.0 / https://codeberg.org/Poslovitch/dejadjvu'
        }
    }, (response) => {
        response.pipe(file);
        file.on('finish', () => { file.close(); });
    });
};

exports.getManifest = async (req: express.Request, res: express.Response) => {
    const folder = './out/' + req.params.id;
    const manifest = await fetch('https://gallica.bnf.fr/iiif/ark:/12148/' + req.params.id + '/manifest.json', {
        headers: {
            'User-Agent': 'Dejadjvu 0.1.0 / https://codeberg.org/Poslovitch/dejadjvu'
        }
    });
    const data: I3FManifest = I3FManifest.parse(await manifest.json());
    const pages = data.sequences[0].canvases;
    if (!fs.existsSync(folder)) { fs.mkdirSync(folder, { recursive: true }); }
    let folio: number = 1;
    for (const page of pages) {
        console.log('Handling page ' + page.label);
        console.log('Fetching ' + page.images[0].resource['@id']);
        const filePath = folder + '/f' + folio + '.jpg';
        await download(page.images[0].resource['@id'], filePath);

        await new Promise(resolve => setTimeout(resolve, 1000));

        // TODO: Sanitize input
        execSync('convert ' + filePath + ' ' + folder + '/f' + folio + '.pnm'); // Convert to PNM
        execSync('c44 ' + folder + '/f' + folio + '.pnm'); // Convert to DJVU

        await new Promise(resolve => setTimeout(resolve, 5000)); // Too many requests...

        folio++;
    }

    res.status(200).send('' + pages.length);
};

exports.getImage = (req: express.Request, res: express.Response) => {
    // https://gallica.bnf.fr/iiif/ark:/12148/btv1b54100464h/f15/full/full/0/native.png
    // TODO: aussi récupérer les thumbnails?
};
