import http from 'http';
import { app } from './app';

const port = process.env.PORT ?? 3000;
app.set('port', port);

const server = http.createServer(app);

server.on('error', errorHandler);
server.on('listening', () => {
    const address = server.address();
    const bind = typeof address === 'string' ? 'bind ' + String(address) : 'port ' + String(port);
    console.log('Listening on ' + bind);
});

function errorHandler (error: NodeJS.ErrnoException) {
    if (error.syscall !== 'listen') {
        throw error;
    }
    switch (error.code) {
        case 'EACCES':
            console.error('Elevated privileges required.');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error('Already in use.');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

server.listen(port);
